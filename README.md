Installation
==============

Clone repository: git clone git@gitlab.com:kindratmakc/editable-tables.git

Copy docker-compose.override.yml.dist, rename it to docker-compose.override.yml

You can remove app_nw and map ports if you need it.

Pull php image if don't to build it locally:
```
docker-compose pull php
```

Start containers:
```
docker-compose up -d
```

Install vendor packages:
```
docker-compose exec --user docker php composer install
```
    

Run migrations(MySQL may not be immediately available):
```
docker-compose exec --user docker php bin/console m:m -n
```
     
If you didn't change anything in docker-compose.override.yml then application will be accessible here:
```
http://172.28.11.2/
```
