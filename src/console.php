<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

$console = new Application('My Silex Application', 'n/a');


$app->register(new \Kurl\Silex\Provider\DoctrineMigrationsProvider($console), [
    'migrations.directory' => __DIR__ . '/../app/Migrations',
    'migrations.namespace' => 'App\Migrations',
]);

$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);

$app->boot();

return $console;
