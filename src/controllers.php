<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

const EDITABLE_TABLE_NAME = 'editable_table';

$extractRow = function (Request $request) {
    return [
        'firstName' => $request->request->get('firstName', ''),
        'lastName'  => $request->request->get('lastName', ''),
        'userName'  => $request->request->get('userName', ''),
    ];
};

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : []);
    }
});

$app->get('/rows', function () use ($app) {
    $rows = $app['db']->fetchAll(
        sprintf('SELECT id, firstName, lastName, userName FROM %s', EDITABLE_TABLE_NAME)
    );

    return $app->json([
        'rows' => $rows,
    ]);
});

$app->post('/rows', function (Request $request) use ($app, $extractRow) {
    $row = $extractRow($request);

    $app['db']->insert(EDITABLE_TABLE_NAME, $row);
    $row['id'] = $app['db']->lastInsertId(EDITABLE_TABLE_NAME);

    return $app->json($row, Response::HTTP_CREATED);
});

$app->put('/rows/{id}', function (Request $request, int $id) use ($app, $extractRow) {
    $row = $extractRow($request);

    $app['db']->update(EDITABLE_TABLE_NAME, $row, [
        'id' => $id,
    ]);
    $row['id'] = $id;

    return $app->json($row);
});

$app->delete('/rows/{id}', function (int $id) use ($app) {
    $app['db']->delete(EDITABLE_TABLE_NAME, [
        'id' => $id,
    ]);

    return $app->json();
});
