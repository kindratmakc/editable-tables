<?php

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170910213708
 *
 * @package App\Migrations
 */
class Version20170910213708 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("CREATE TABLE editable_table (
                          id INT UNSIGNED AUTO_INCREMENT NOT NULL,
                          firstName LONGTEXT DEFAULT NULL,
                          lastName LONGTEXT DEFAULT NULL,
                          userName LONGTEXT DEFAULT NULL,
                          PRIMARY KEY(id))
                          DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("INSERT INTO editable_table (firstName, lastName, userName) VALUES
                          ('test_data_1', 'test_data_2', 'test_data_3'),
                          ('test_data_4', 'test_data_5', 'test_data_6'),
                          ('test_data_7', 'test_data_8', 'test_data_9')
                      ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE editable_table');
    }
}
