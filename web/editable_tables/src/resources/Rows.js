import Vue from 'vue';

export default Vue.resource('rows{/id}', {}, {
    all: {
        method: 'GET',
        url: 'rows',
    },
}, {
    root: 'app.php',
});
